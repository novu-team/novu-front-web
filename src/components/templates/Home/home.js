import React from 'react'

import HomeIntro from '../../organisms/homeIntro'

const Home = () => (
  <>
    <HomeIntro />
  </>
)

export default Home