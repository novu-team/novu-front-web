const theme = {
  primary: '#008A92FF',
  secondary: '#CBCBCBFF',
  darkGrey: '#808080FF',
  brightGrey: '#F1F1F1FF',
  grey: '#666666FF',
  alert: '#CF0023FF',
  validateDate: '#FBAE17FF',
  transparent: 'transparent',
  white: '#fff'
}

export default theme